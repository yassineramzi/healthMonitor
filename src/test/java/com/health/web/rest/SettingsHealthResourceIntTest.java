package com.health.web.rest;

import com.health.HealthMonitorApp;

import com.health.domain.SettingsHealth;
import com.health.repository.SettingsHealthRepository;
import com.health.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SettingsHealthResource REST controller.
 *
 * @see SettingsHealthResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HealthMonitorApp.class)
public class SettingsHealthResourceIntTest {

    private static final Long DEFAULT_WEEKLY_GOAL = 1L;
    private static final Long UPDATED_WEEKLY_GOAL = 2L;

    private static final String DEFAULT_WEIGHT_UNITS = "AAAAAAAAAA";
    private static final String UPDATED_WEIGHT_UNITS = "BBBBBBBBBB";

    @Autowired
    private SettingsHealthRepository settingsHealthRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSettingsHealthMockMvc;

    private SettingsHealth settingsHealth;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SettingsHealthResource settingsHealthResource = new SettingsHealthResource(settingsHealthRepository);
        this.restSettingsHealthMockMvc = MockMvcBuilders.standaloneSetup(settingsHealthResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SettingsHealth createEntity(EntityManager em) {
        SettingsHealth settingsHealth = new SettingsHealth()
            .weeklyGoal(DEFAULT_WEEKLY_GOAL)
            .weightUnits(DEFAULT_WEIGHT_UNITS);
        return settingsHealth;
    }

    @Before
    public void initTest() {
        settingsHealth = createEntity(em);
    }

    @Test
    @Transactional
    public void createSettingsHealth() throws Exception {
        int databaseSizeBeforeCreate = settingsHealthRepository.findAll().size();

        // Create the SettingsHealth
        restSettingsHealthMockMvc.perform(post("/api/settings-healths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(settingsHealth)))
            .andExpect(status().isCreated());

        // Validate the SettingsHealth in the database
        List<SettingsHealth> settingsHealthList = settingsHealthRepository.findAll();
        assertThat(settingsHealthList).hasSize(databaseSizeBeforeCreate + 1);
        SettingsHealth testSettingsHealth = settingsHealthList.get(settingsHealthList.size() - 1);
        assertThat(testSettingsHealth.getWeeklyGoal()).isEqualTo(DEFAULT_WEEKLY_GOAL);
        assertThat(testSettingsHealth.getWeightUnits()).isEqualTo(DEFAULT_WEIGHT_UNITS);
    }

    @Test
    @Transactional
    public void createSettingsHealthWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = settingsHealthRepository.findAll().size();

        // Create the SettingsHealth with an existing ID
        settingsHealth.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSettingsHealthMockMvc.perform(post("/api/settings-healths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(settingsHealth)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<SettingsHealth> settingsHealthList = settingsHealthRepository.findAll();
        assertThat(settingsHealthList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSettingsHealths() throws Exception {
        // Initialize the database
        settingsHealthRepository.saveAndFlush(settingsHealth);

        // Get all the settingsHealthList
        restSettingsHealthMockMvc.perform(get("/api/settings-healths?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(settingsHealth.getId().intValue())))
            .andExpect(jsonPath("$.[*].weeklyGoal").value(hasItem(DEFAULT_WEEKLY_GOAL.intValue())))
            .andExpect(jsonPath("$.[*].weightUnits").value(hasItem(DEFAULT_WEIGHT_UNITS.toString())));
    }

    @Test
    @Transactional
    public void getSettingsHealth() throws Exception {
        // Initialize the database
        settingsHealthRepository.saveAndFlush(settingsHealth);

        // Get the settingsHealth
        restSettingsHealthMockMvc.perform(get("/api/settings-healths/{id}", settingsHealth.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(settingsHealth.getId().intValue()))
            .andExpect(jsonPath("$.weeklyGoal").value(DEFAULT_WEEKLY_GOAL.intValue()))
            .andExpect(jsonPath("$.weightUnits").value(DEFAULT_WEIGHT_UNITS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSettingsHealth() throws Exception {
        // Get the settingsHealth
        restSettingsHealthMockMvc.perform(get("/api/settings-healths/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSettingsHealth() throws Exception {
        // Initialize the database
        settingsHealthRepository.saveAndFlush(settingsHealth);
        int databaseSizeBeforeUpdate = settingsHealthRepository.findAll().size();

        // Update the settingsHealth
        SettingsHealth updatedSettingsHealth = settingsHealthRepository.findOne(settingsHealth.getId());
        updatedSettingsHealth
            .weeklyGoal(UPDATED_WEEKLY_GOAL)
            .weightUnits(UPDATED_WEIGHT_UNITS);

        restSettingsHealthMockMvc.perform(put("/api/settings-healths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSettingsHealth)))
            .andExpect(status().isOk());

        // Validate the SettingsHealth in the database
        List<SettingsHealth> settingsHealthList = settingsHealthRepository.findAll();
        assertThat(settingsHealthList).hasSize(databaseSizeBeforeUpdate);
        SettingsHealth testSettingsHealth = settingsHealthList.get(settingsHealthList.size() - 1);
        assertThat(testSettingsHealth.getWeeklyGoal()).isEqualTo(UPDATED_WEEKLY_GOAL);
        assertThat(testSettingsHealth.getWeightUnits()).isEqualTo(UPDATED_WEIGHT_UNITS);
    }

    @Test
    @Transactional
    public void updateNonExistingSettingsHealth() throws Exception {
        int databaseSizeBeforeUpdate = settingsHealthRepository.findAll().size();

        // Create the SettingsHealth

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSettingsHealthMockMvc.perform(put("/api/settings-healths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(settingsHealth)))
            .andExpect(status().isCreated());

        // Validate the SettingsHealth in the database
        List<SettingsHealth> settingsHealthList = settingsHealthRepository.findAll();
        assertThat(settingsHealthList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSettingsHealth() throws Exception {
        // Initialize the database
        settingsHealthRepository.saveAndFlush(settingsHealth);
        int databaseSizeBeforeDelete = settingsHealthRepository.findAll().size();

        // Get the settingsHealth
        restSettingsHealthMockMvc.perform(delete("/api/settings-healths/{id}", settingsHealth.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SettingsHealth> settingsHealthList = settingsHealthRepository.findAll();
        assertThat(settingsHealthList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SettingsHealth.class);
        SettingsHealth settingsHealth1 = new SettingsHealth();
        settingsHealth1.setId(1L);
        SettingsHealth settingsHealth2 = new SettingsHealth();
        settingsHealth2.setId(settingsHealth1.getId());
        assertThat(settingsHealth1).isEqualTo(settingsHealth2);
        settingsHealth2.setId(2L);
        assertThat(settingsHealth1).isNotEqualTo(settingsHealth2);
        settingsHealth1.setId(null);
        assertThat(settingsHealth1).isNotEqualTo(settingsHealth2);
    }
}
