package com.health.web.rest;

import com.health.HealthMonitorApp;

import com.health.domain.UserHealth;
import com.health.repository.UserHealthRepository;
import com.health.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserHealthResource REST controller.
 *
 * @see UserHealthResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HealthMonitorApp.class)
public class UserHealthResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    @Autowired
    private UserHealthRepository userHealthRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserHealthMockMvc;

    private UserHealth userHealth;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserHealthResource userHealthResource = new UserHealthResource(userHealthRepository);
        this.restUserHealthMockMvc = MockMvcBuilders.standaloneSetup(userHealthResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserHealth createEntity(EntityManager em) {
        UserHealth userHealth = new UserHealth()
            .name(DEFAULT_NAME)
            .email(DEFAULT_EMAIL);
        return userHealth;
    }

    @Before
    public void initTest() {
        userHealth = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserHealth() throws Exception {
        int databaseSizeBeforeCreate = userHealthRepository.findAll().size();

        // Create the UserHealth
        restUserHealthMockMvc.perform(post("/api/user-healths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userHealth)))
            .andExpect(status().isCreated());

        // Validate the UserHealth in the database
        List<UserHealth> userHealthList = userHealthRepository.findAll();
        assertThat(userHealthList).hasSize(databaseSizeBeforeCreate + 1);
        UserHealth testUserHealth = userHealthList.get(userHealthList.size() - 1);
        assertThat(testUserHealth.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testUserHealth.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    public void createUserHealthWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userHealthRepository.findAll().size();

        // Create the UserHealth with an existing ID
        userHealth.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserHealthMockMvc.perform(post("/api/user-healths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userHealth)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UserHealth> userHealthList = userHealthRepository.findAll();
        assertThat(userHealthList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUserHealths() throws Exception {
        // Initialize the database
        userHealthRepository.saveAndFlush(userHealth);

        // Get all the userHealthList
        restUserHealthMockMvc.perform(get("/api/user-healths?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userHealth.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())));
    }

    @Test
    @Transactional
    public void getUserHealth() throws Exception {
        // Initialize the database
        userHealthRepository.saveAndFlush(userHealth);

        // Get the userHealth
        restUserHealthMockMvc.perform(get("/api/user-healths/{id}", userHealth.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userHealth.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserHealth() throws Exception {
        // Get the userHealth
        restUserHealthMockMvc.perform(get("/api/user-healths/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserHealth() throws Exception {
        // Initialize the database
        userHealthRepository.saveAndFlush(userHealth);
        int databaseSizeBeforeUpdate = userHealthRepository.findAll().size();

        // Update the userHealth
        UserHealth updatedUserHealth = userHealthRepository.findOne(userHealth.getId());
        updatedUserHealth
            .name(UPDATED_NAME)
            .email(UPDATED_EMAIL);

        restUserHealthMockMvc.perform(put("/api/user-healths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUserHealth)))
            .andExpect(status().isOk());

        // Validate the UserHealth in the database
        List<UserHealth> userHealthList = userHealthRepository.findAll();
        assertThat(userHealthList).hasSize(databaseSizeBeforeUpdate);
        UserHealth testUserHealth = userHealthList.get(userHealthList.size() - 1);
        assertThat(testUserHealth.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testUserHealth.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void updateNonExistingUserHealth() throws Exception {
        int databaseSizeBeforeUpdate = userHealthRepository.findAll().size();

        // Create the UserHealth

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserHealthMockMvc.perform(put("/api/user-healths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userHealth)))
            .andExpect(status().isCreated());

        // Validate the UserHealth in the database
        List<UserHealth> userHealthList = userHealthRepository.findAll();
        assertThat(userHealthList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUserHealth() throws Exception {
        // Initialize the database
        userHealthRepository.saveAndFlush(userHealth);
        int databaseSizeBeforeDelete = userHealthRepository.findAll().size();

        // Get the userHealth
        restUserHealthMockMvc.perform(delete("/api/user-healths/{id}", userHealth.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserHealth> userHealthList = userHealthRepository.findAll();
        assertThat(userHealthList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserHealth.class);
        UserHealth userHealth1 = new UserHealth();
        userHealth1.setId(1L);
        UserHealth userHealth2 = new UserHealth();
        userHealth2.setId(userHealth1.getId());
        assertThat(userHealth1).isEqualTo(userHealth2);
        userHealth2.setId(2L);
        assertThat(userHealth1).isNotEqualTo(userHealth2);
        userHealth1.setId(null);
        assertThat(userHealth1).isNotEqualTo(userHealth2);
    }
}
