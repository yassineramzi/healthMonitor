'use strict';

describe('Controller Tests', function() {

    describe('SettingsHealth Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockSettingsHealth, MockUserHealth;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockSettingsHealth = jasmine.createSpy('MockSettingsHealth');
            MockUserHealth = jasmine.createSpy('MockUserHealth');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'SettingsHealth': MockSettingsHealth,
                'UserHealth': MockUserHealth
            };
            createController = function() {
                $injector.get('$controller')("SettingsHealthDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'healthMonitorApp:settingsHealthUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
