(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .controller('WeightDetailController', WeightDetailController);

    WeightDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Weight', 'UserHealth'];

    function WeightDetailController($scope, $rootScope, $stateParams, previousState, entity, Weight, UserHealth) {
        var vm = this;

        vm.weight = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('healthMonitorApp:weightUpdate', function(event, result) {
            vm.weight = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
