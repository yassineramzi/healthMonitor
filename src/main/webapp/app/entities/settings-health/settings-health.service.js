(function() {
    'use strict';
    angular
        .module('healthMonitorApp')
        .factory('SettingsHealth', SettingsHealth);

    SettingsHealth.$inject = ['$resource'];

    function SettingsHealth ($resource) {
        var resourceUrl =  'api/settings-healths/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
