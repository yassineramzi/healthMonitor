(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .controller('SettingsHealthDialogController', SettingsHealthDialogController);

    SettingsHealthDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'SettingsHealth', 'UserHealth'];

    function SettingsHealthDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, SettingsHealth, UserHealth) {
        var vm = this;

        vm.settingsHealth = entity;
        vm.clear = clear;
        vm.save = save;
        vm.userhealths = UserHealth.query({filter: 'settingshealth-is-null'});
        $q.all([vm.settingsHealth.$promise, vm.userhealths.$promise]).then(function() {
            if (!vm.settingsHealth.userHealth || !vm.settingsHealth.userHealth.id) {
                return $q.reject();
            }
            return UserHealth.get({id : vm.settingsHealth.userHealth.id}).$promise;
        }).then(function(userHealth) {
            vm.userhealths.push(userHealth);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.settingsHealth.id !== null) {
                SettingsHealth.update(vm.settingsHealth, onSaveSuccess, onSaveError);
            } else {
                SettingsHealth.save(vm.settingsHealth, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('healthMonitorApp:settingsHealthUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
