(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .controller('SettingsHealthDeleteController',SettingsHealthDeleteController);

    SettingsHealthDeleteController.$inject = ['$uibModalInstance', 'entity', 'SettingsHealth'];

    function SettingsHealthDeleteController($uibModalInstance, entity, SettingsHealth) {
        var vm = this;

        vm.settingsHealth = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SettingsHealth.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
