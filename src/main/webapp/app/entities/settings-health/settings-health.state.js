(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('settings-health', {
            parent: 'entity',
            url: '/settings-health',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'healthMonitorApp.settingsHealth.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/settings-health/settings-healths.html',
                    controller: 'SettingsHealthController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('settingsHealth');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('settings-health-detail', {
            parent: 'settings-health',
            url: '/settings-health/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'healthMonitorApp.settingsHealth.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/settings-health/settings-health-detail.html',
                    controller: 'SettingsHealthDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('settingsHealth');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SettingsHealth', function($stateParams, SettingsHealth) {
                    return SettingsHealth.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'settings-health',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('settings-health-detail.edit', {
            parent: 'settings-health-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/settings-health/settings-health-dialog.html',
                    controller: 'SettingsHealthDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SettingsHealth', function(SettingsHealth) {
                            return SettingsHealth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('settings-health.new', {
            parent: 'settings-health',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/settings-health/settings-health-dialog.html',
                    controller: 'SettingsHealthDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                weeklyGoal: null,
                                weightUnits: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('settings-health', null, { reload: 'settings-health' });
                }, function() {
                    $state.go('settings-health');
                });
            }]
        })
        .state('settings-health.edit', {
            parent: 'settings-health',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/settings-health/settings-health-dialog.html',
                    controller: 'SettingsHealthDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SettingsHealth', function(SettingsHealth) {
                            return SettingsHealth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('settings-health', null, { reload: 'settings-health' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('settings-health.delete', {
            parent: 'settings-health',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/settings-health/settings-health-delete-dialog.html',
                    controller: 'SettingsHealthDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SettingsHealth', function(SettingsHealth) {
                            return SettingsHealth.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('settings-health', null, { reload: 'settings-health' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
