(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .controller('SettingsHealthDetailController', SettingsHealthDetailController);

    SettingsHealthDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'SettingsHealth', 'UserHealth'];

    function SettingsHealthDetailController($scope, $rootScope, $stateParams, previousState, entity, SettingsHealth, UserHealth) {
        var vm = this;

        vm.settingsHealth = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('healthMonitorApp:settingsHealthUpdate', function(event, result) {
            vm.settingsHealth = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
