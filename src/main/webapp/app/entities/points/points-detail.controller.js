(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .controller('PointsDetailController', PointsDetailController);

    PointsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Points', 'UserHealth'];

    function PointsDetailController($scope, $rootScope, $stateParams, previousState, entity, Points, UserHealth) {
        var vm = this;

        vm.points = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('healthMonitorApp:pointsUpdate', function(event, result) {
            vm.points = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
