(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .controller('PointsDialogController', PointsDialogController);

    PointsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Points', 'UserHealth'];

    function PointsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Points, UserHealth) {
        var vm = this;

        vm.points = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.userhealths = UserHealth.query({filter: 'points-is-null'});
        $q.all([vm.points.$promise, vm.userhealths.$promise]).then(function() {
            if (!vm.points.userHealth || !vm.points.userHealth.id) {
                return $q.reject();
            }
            return UserHealth.get({id : vm.points.userHealth.id}).$promise;
        }).then(function(userHealth) {
            vm.userhealths.push(userHealth);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.points.id !== null) {
                Points.update(vm.points, onSaveSuccess, onSaveError);
            } else {
                Points.save(vm.points, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('healthMonitorApp:pointsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.date = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
