(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .controller('BloodPressureDetailController', BloodPressureDetailController);

    BloodPressureDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'BloodPressure', 'UserHealth'];

    function BloodPressureDetailController($scope, $rootScope, $stateParams, previousState, entity, BloodPressure, UserHealth) {
        var vm = this;

        vm.bloodPressure = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('healthMonitorApp:bloodPressureUpdate', function(event, result) {
            vm.bloodPressure = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
