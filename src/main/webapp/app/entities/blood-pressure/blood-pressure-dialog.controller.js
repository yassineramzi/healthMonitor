(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .controller('BloodPressureDialogController', BloodPressureDialogController);

    BloodPressureDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'BloodPressure', 'UserHealth'];

    function BloodPressureDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, BloodPressure, UserHealth) {
        var vm = this;

        vm.bloodPressure = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.userhealths = UserHealth.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.bloodPressure.id !== null) {
                BloodPressure.update(vm.bloodPressure, onSaveSuccess, onSaveError);
            } else {
                BloodPressure.save(vm.bloodPressure, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('healthMonitorApp:bloodPressureUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.date = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
