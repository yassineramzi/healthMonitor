(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .controller('UserHealthDialogController', UserHealthDialogController);

    UserHealthDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserHealth', 'Points'];

    function UserHealthDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserHealth, Points) {
        var vm = this;

        vm.userHealth = entity;
        vm.clear = clear;
        vm.save = save;
        vm.points = Points.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userHealth.id !== null) {
                UserHealth.update(vm.userHealth, onSaveSuccess, onSaveError);
            } else {
                UserHealth.save(vm.userHealth, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('healthMonitorApp:userHealthUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
