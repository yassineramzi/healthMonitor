(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .controller('UserHealthDetailController', UserHealthDetailController);

    UserHealthDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserHealth', 'Points'];

    function UserHealthDetailController($scope, $rootScope, $stateParams, previousState, entity, UserHealth, Points) {
        var vm = this;

        vm.userHealth = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('healthMonitorApp:userHealthUpdate', function(event, result) {
            vm.userHealth = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
