(function() {
    'use strict';

    angular
        .module('healthMonitorApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
