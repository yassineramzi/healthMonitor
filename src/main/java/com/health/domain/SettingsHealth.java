package com.health.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SettingsHealth.
 */
@Entity
@Table(name = "settings_health")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SettingsHealth implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "weekly_goal")
    private Long weeklyGoal;

    @Column(name = "weight_units")
    private String weightUnits;

    @OneToOne
    @JoinColumn(unique = true)
    private UserHealth userHealth;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWeeklyGoal() {
        return weeklyGoal;
    }

    public SettingsHealth weeklyGoal(Long weeklyGoal) {
        this.weeklyGoal = weeklyGoal;
        return this;
    }

    public void setWeeklyGoal(Long weeklyGoal) {
        this.weeklyGoal = weeklyGoal;
    }

    public String getWeightUnits() {
        return weightUnits;
    }

    public SettingsHealth weightUnits(String weightUnits) {
        this.weightUnits = weightUnits;
        return this;
    }

    public void setWeightUnits(String weightUnits) {
        this.weightUnits = weightUnits;
    }

    public UserHealth getUserHealth() {
        return userHealth;
    }

    public SettingsHealth userHealth(UserHealth userHealth) {
        this.userHealth = userHealth;
        return this;
    }

    public void setUserHealth(UserHealth userHealth) {
        this.userHealth = userHealth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SettingsHealth settingsHealth = (SettingsHealth) o;
        if (settingsHealth.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), settingsHealth.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SettingsHealth{" +
            "id=" + getId() +
            ", weeklyGoal='" + getWeeklyGoal() + "'" +
            ", weightUnits='" + getWeightUnits() + "'" +
            "}";
    }
}
