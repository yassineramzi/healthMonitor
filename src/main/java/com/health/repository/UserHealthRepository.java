package com.health.repository;

import com.health.domain.UserHealth;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the UserHealth entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserHealthRepository extends JpaRepository<UserHealth,Long> {

}
