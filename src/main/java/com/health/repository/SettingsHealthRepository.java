package com.health.repository;

import com.health.domain.SettingsHealth;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SettingsHealth entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SettingsHealthRepository extends JpaRepository<SettingsHealth,Long> {

}
