package com.health.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.health.domain.UserHealth;

import com.health.repository.UserHealthRepository;
import com.health.web.rest.util.HeaderUtil;
import com.health.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing UserHealth.
 */
@RestController
@RequestMapping("/api")
public class UserHealthResource {

    private final Logger log = LoggerFactory.getLogger(UserHealthResource.class);

    private static final String ENTITY_NAME = "userHealth";

    private final UserHealthRepository userHealthRepository;

    public UserHealthResource(UserHealthRepository userHealthRepository) {
        this.userHealthRepository = userHealthRepository;
    }

    /**
     * POST  /user-healths : Create a new userHealth.
     *
     * @param userHealth the userHealth to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userHealth, or with status 400 (Bad Request) if the userHealth has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-healths")
    @Timed
    public ResponseEntity<UserHealth> createUserHealth(@RequestBody UserHealth userHealth) throws URISyntaxException {
        log.debug("REST request to save UserHealth : {}", userHealth);
        if (userHealth.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new userHealth cannot already have an ID")).body(null);
        }
        UserHealth result = userHealthRepository.save(userHealth);
        return ResponseEntity.created(new URI("/api/user-healths/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-healths : Updates an existing userHealth.
     *
     * @param userHealth the userHealth to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userHealth,
     * or with status 400 (Bad Request) if the userHealth is not valid,
     * or with status 500 (Internal Server Error) if the userHealth couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-healths")
    @Timed
    public ResponseEntity<UserHealth> updateUserHealth(@RequestBody UserHealth userHealth) throws URISyntaxException {
        log.debug("REST request to update UserHealth : {}", userHealth);
        if (userHealth.getId() == null) {
            return createUserHealth(userHealth);
        }
        UserHealth result = userHealthRepository.save(userHealth);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userHealth.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-healths : get all the userHealths.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of userHealths in body
     */
    @GetMapping("/user-healths")
    @Timed
    public ResponseEntity<List<UserHealth>> getAllUserHealths(@ApiParam Pageable pageable, @RequestParam(required = false) String filter) {
        if ("points-is-null".equals(filter)) {
            log.debug("REST request to get all UserHealths where points is null");
            return new ResponseEntity<>(StreamSupport
                .stream(userHealthRepository.findAll().spliterator(), false)
                .filter(userHealth -> userHealth.getPoints() == null)
                .collect(Collectors.toList()), HttpStatus.OK);
        }
        log.debug("REST request to get a page of UserHealths");
        Page<UserHealth> page = userHealthRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-healths");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-healths/:id : get the "id" userHealth.
     *
     * @param id the id of the userHealth to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userHealth, or with status 404 (Not Found)
     */
    @GetMapping("/user-healths/{id}")
    @Timed
    public ResponseEntity<UserHealth> getUserHealth(@PathVariable Long id) {
        log.debug("REST request to get UserHealth : {}", id);
        UserHealth userHealth = userHealthRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userHealth));
    }

    /**
     * DELETE  /user-healths/:id : delete the "id" userHealth.
     *
     * @param id the id of the userHealth to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-healths/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserHealth(@PathVariable Long id) {
        log.debug("REST request to delete UserHealth : {}", id);
        userHealthRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
