package com.health.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.health.domain.SettingsHealth;

import com.health.repository.SettingsHealthRepository;
import com.health.web.rest.util.HeaderUtil;
import com.health.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SettingsHealth.
 */
@RestController
@RequestMapping("/api")
public class SettingsHealthResource {

    private final Logger log = LoggerFactory.getLogger(SettingsHealthResource.class);

    private static final String ENTITY_NAME = "settingsHealth";

    private final SettingsHealthRepository settingsHealthRepository;

    public SettingsHealthResource(SettingsHealthRepository settingsHealthRepository) {
        this.settingsHealthRepository = settingsHealthRepository;
    }

    /**
     * POST  /settings-healths : Create a new settingsHealth.
     *
     * @param settingsHealth the settingsHealth to create
     * @return the ResponseEntity with status 201 (Created) and with body the new settingsHealth, or with status 400 (Bad Request) if the settingsHealth has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/settings-healths")
    @Timed
    public ResponseEntity<SettingsHealth> createSettingsHealth(@RequestBody SettingsHealth settingsHealth) throws URISyntaxException {
        log.debug("REST request to save SettingsHealth : {}", settingsHealth);
        if (settingsHealth.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new settingsHealth cannot already have an ID")).body(null);
        }
        SettingsHealth result = settingsHealthRepository.save(settingsHealth);
        return ResponseEntity.created(new URI("/api/settings-healths/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /settings-healths : Updates an existing settingsHealth.
     *
     * @param settingsHealth the settingsHealth to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated settingsHealth,
     * or with status 400 (Bad Request) if the settingsHealth is not valid,
     * or with status 500 (Internal Server Error) if the settingsHealth couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/settings-healths")
    @Timed
    public ResponseEntity<SettingsHealth> updateSettingsHealth(@RequestBody SettingsHealth settingsHealth) throws URISyntaxException {
        log.debug("REST request to update SettingsHealth : {}", settingsHealth);
        if (settingsHealth.getId() == null) {
            return createSettingsHealth(settingsHealth);
        }
        SettingsHealth result = settingsHealthRepository.save(settingsHealth);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, settingsHealth.getId().toString()))
            .body(result);
    }

    /**
     * GET  /settings-healths : get all the settingsHealths.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of settingsHealths in body
     */
    @GetMapping("/settings-healths")
    @Timed
    public ResponseEntity<List<SettingsHealth>> getAllSettingsHealths(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of SettingsHealths");
        Page<SettingsHealth> page = settingsHealthRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/settings-healths");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /settings-healths/:id : get the "id" settingsHealth.
     *
     * @param id the id of the settingsHealth to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the settingsHealth, or with status 404 (Not Found)
     */
    @GetMapping("/settings-healths/{id}")
    @Timed
    public ResponseEntity<SettingsHealth> getSettingsHealth(@PathVariable Long id) {
        log.debug("REST request to get SettingsHealth : {}", id);
        SettingsHealth settingsHealth = settingsHealthRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(settingsHealth));
    }

    /**
     * DELETE  /settings-healths/:id : delete the "id" settingsHealth.
     *
     * @param id the id of the settingsHealth to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/settings-healths/{id}")
    @Timed
    public ResponseEntity<Void> deleteSettingsHealth(@PathVariable Long id) {
        log.debug("REST request to delete SettingsHealth : {}", id);
        settingsHealthRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
